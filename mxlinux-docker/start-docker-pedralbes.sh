#!/bin/bash
# ----------------------------------
# Variables
# ----------------------------------
puertoAnadirSwarm="2377"
portainerPedralbesYML="portainer-pedralbes.yml"
dominioPortainerPedralbes="port.pedralbes.cat"
# ----------------------------------
# Colores
# ----------------------------------
colorDefecto=$'\e[0m'
colorAzul=$'\e[1;34m'
colorRojo=$'\e[1;31m'
# ----------------------------------
# Funciones
# ----------------------------------
# Pausar el script hasta presionar [Enter]
pausar(){
	read -p "Presiona [Enter] para continuar..." fackEnterKey
}
# Comprobamos que el campo no esté vacío
comprobar_vacio(){
    if [[ -n "$1" ]]; then return 0; else return 1; fi
}
# Mostramos las máquinas en Docker Machine
mostrar_maquinas_dm(){
    docker-machine ls
    printf "\n"
    pausar
}
# Generar las claves ssh del master
generar_claves_master(){
    ssh-keygen -t rsa -f ~/.ssh/id_rsa -q -P ""
    if [[ $? -eq 0 ]]; then
        printf "%s\n" "${colorAzul}Clave ssh generada correctamente${colorDefecto}"
    fi
    printf "\n"
    pausar
}
# Copiar la clave ssh del master a un nodo
copiar_clave_master_nodo(){
    # Obtenemos la IP de la máquina
    read -p "IP de la máquina: " ipMaquina
    # Obtenemos el nombre de usuario
    read -p "Usuario ssh de la máquina: " usuarioMaquina
    # Comprobamos que haya introducido la IP
    comprobar_vacio $ipMaquina
    if [[ $? -eq 0 ]]; then
        # Comprobamos que haya introducido el usuario
        comprobar_vacio $usuarioMaquina
        if [[ $? -eq 0 ]]; then
            printf "%s\n" "${colorAzul}Copiando clave ssh al nodo...${colorDefecto}"
            ssh-copy-id -o StrictHostKeyChecking=no $usuarioMaquina@$ipMaquina
            printf "\n"  
        else
            printf "%s\n" "${colorRojo}El usuario no puede estar vacío${colorDefecto}"
        fi        
    else
        printf "%s\n" "${colorRojo}La IP no puede estar vacía${colorDefecto}"
    fi
	pausar
}
# Añadir el nodo a Docker Machine
anadir_nodo_a_dm(){
    # Obtenemos la IP de la máquina
    read -p "IP de la máquina: " ipMaquina
    # Obtenemos el nombre de usuario
    read -p "Usuario ssh de la máquina: " usuarioMaquina
    # Obtenemos el nombre de la máquina
    read -p "Nombre de la máquina: " nombreMaquina
    # Comprobamos que haya introducido la IP
    comprobar_vacio $ipMaquina
    if [[ $? -eq 0 ]]; then
        # Comprobamos que haya introducido el usuario
        comprobar_vacio $usuarioMaquina
        if [[ $? -eq 0 ]]; then
            # Comprobamos que haya introducido el nombre de la máquina
            comprobar_vacio $nombreMaquina
            if [[ $? -eq 0 ]]; then
                printf "%s\n" "${colorAzul}Añadiendo la máquina $nombreMaquina como Docker Machine...${colorDefecto}"
                docker-machine create \
                    --driver generic \
                    --generic-ip-address=$ipMaquina \
                    --generic-ssh-key ~/.ssh/id_rsa \
                    --generic-ssh-user=$usuarioMaquina \
            $nombreMaquina
            else
                printf "%s\n" "${colorRojo}El nombre del nodo no puede estar vacío${colorDefecto}"
            fi
        else
            printf "%s\n" "${colorRojo}El usuario no puede estar vacío${colorDefecto}"
        fi        
    else
        printf "%s\n" "${colorRojo}La IP no puede estar vacía${colorDefecto}"
    fi
	pausar
}
# Corregir los certificados para Docker Machine
corregir_certificados_dm(){
    # Obtenemos el nombre de la máquina
    read -p "Nombre de la máquina: " nombreMaquina
    # Comprobamos que haya introducido el nombre de la máquina
    comprobar_vacio $nombreMaquina
    if [[ $? -eq 0 ]]; then
        # Comprobamos que exista esa máquina en Docker Machine
        ipMaquina=$(docker-machine ip $nombreMaquina 2> /dev/null)
        if [[ $? -eq 0 ]]; then
            printf "%s\n" "${colorAzul}Creando certificados de $nombreMaquina...${colorDefecto}"
            cp ~/.docker/machine/certs/ca.pem ~/.docker/machine/machines/$nombreMaquina/ca.pem
            cp ~/.docker/machine/certs/key.pem ~/.docker/machine/machines/$nombreMaquina/key.pem
            cp ~/.docker/machine/certs/cert.pem ~/.docker/machine/machines/$nombreMaquina/cert.pem
            cd ~/.docker/machine/machines/$nombreMaquina
            openssl genrsa -out server-key.pem 4096
            openssl req -subj "/CN=$nombreMaquina" -sha256 -new -key server-key.pem -out server.csr
            openssl x509 -req -days 3650 -in server.csr -CA ~/.docker/machine/certs/ca.pem -CAkey ~/.docker/machine/certs/ca-key.pem -out server.pem
            echo subjectAltName = IP:$ipMaquina,IP:127.0.0.1 > extfile.cnf
            echo extendedKeyUsage = serverAuth >> extfile.cnf
            openssl x509 -req -days 3650 -sha256 -in server.csr -CA ~/.docker/machine/certs/ca.pem -CAkey ~/.docker/machine/certs/ca-key.pem -CAcreateserial -out server.pem -extfile extfile.cnf
            rm extfile.cnf server.csr
            printf "%s\n" "${colorAzul}Copiando certificados a $nombreMaquina...${colorDefecto}"
            docker-machine scp ~/.docker/machine/machines/$nombreMaquina/ca.pem $nombreMaquina:/etc/docker/ca.pem
            docker-machine scp ~/.docker/machine/machines/$nombreMaquina/server.pem $nombreMaquina:/etc/docker/server.pem
            docker-machine scp ~/.docker/machine/machines/$nombreMaquina/server-key.pem $nombreMaquina:/etc/docker/server-key.pem
            printf "%s\n" "${colorAzul}Reiniciando el servicio de $nombreMaquina...${colorDefecto}"
            docker-machine ssh $nombreMaquina "
                echo \"
                    DOCKER_OPTS='
                    -H tcp://0.0.0.0:2376
                    -H unix:///var/run/docker.sock
                    --tlsverify
                    --tlscacert /etc/docker/ca.pem
                    --tlscert /etc/docker/server.pem
                    --tlskey /etc/docker/server-key.pem
                    --label provider=generic
                    '
                    \" > /etc/conf.d/docker
                    "
            docker-machine ssh $nombreMaquina "service docker restart"
            printf "%s\n" "${colorAzul}Certificados de $nombreMaquina corregidos correctamente${colorDefecto}"
        else
            printf "%s\n" "${colorRojo}No existe $nombreMaquina en Docker Machine${colorDefecto}"
        fi
    else
        printf "%s\n" "${colorRojo}El nombre de la máquina no puede estar vacío${colorDefecto}"
    fi
	pausar
}
# Inicializar Swarm en un manager
inicializar_swarm(){
    # Obtenemos el nombre de la máquina
    read -p "Nombre de la máquina: " nombreMaquina
    # Comprobamos que haya introducido el nombre de la máquina
    comprobar_vacio $nombreMaquina
    if [[ $? -eq 0 ]]; then
            # Comprobamos que exista esa máquina en Docker Machine
            ipMaquinaDM=$(docker-machine ip $nombreMaquina 2> /dev/null)
            if [[ $? -eq 0 ]]; then
                printf "%s\n" "${colorAzul}Inicializando Swarm en $nombreMaquina...${colorDefecto}"
                docker-machine ssh $nombreMaquina "docker swarm init --advertise-addr $ipMaquinaDM"
            else
                printf "%s\n" "${colorRojo}No existe $nombreMaquina en Docker Machine${colorDefecto}"    
            fi
    else
        printf "%s\n" "${colorRojo}El nombre de la máquina no puede estar vacío${colorDefecto}"
    fi
	pausar   
}
# Añadir un worker en Swarm
anadir_worker_swarm(){
    # Obtenemos el nombre de la máquina manager
    read -p "Nombre de la máquina manager: " nombreMaquinaManager
    # Obtenemos el nombre de la máquina worker
    read -p "Nombre de la máquina worker: " nombreMaquinaWorker
    # Comprobamos que haya introducido el nombre de la máquina manager
    comprobar_vacio $nombreMaquinaManager
    if [[ $? -eq 0 ]]; then
        # Comprobamos que haya introducido el nombre de la máquina manager
        comprobar_vacio $nombreMaquinaWorker
        if [[ $? -eq 0 ]]; then
            # Comprobamos que exista el manager en Docker Machine
            ipMaquinaManager=$(docker-machine ip $nombreMaquinaManager 2> /dev/null)
            if [[ $? -eq 0 ]]; then
                # Comprobamos que exista el worker en Docker Machine
                ipMaquinaWorker=$(docker-machine ip $nombreMaquinaWorker 2> /dev/null)
                if [[ $? -eq 0 ]]; then
                    printf "%s\n" "${colorAzul}Añadiendo worker $nombreMaquinaWorker en el Swarm de $nombreMaquinaManager...${colorDefecto}"
                    tokenMaquinaManager=$(docker-machine ssh $nombreMaquinaManager "docker swarm join-token worker -q")
                    docker-machine ssh $nombreMaquinaWorker "docker swarm join --token $tokenMaquinaManager $ipMaquinaManager:$puertoAnadirSwarm"
                else
                    printf "%s\n" "${colorRojo}No existe $nombreMaquinaWorker en Docker Machine${colorDefecto}"    
                fi
            else
                printf "%s\n" "${colorRojo}No existe $nombreMaquinaManager en Docker Machine${colorDefecto}"    
            fi  
        else
            printf "%s\n" "${colorRojo}El nombre del worker no puede estar vacío${colorDefecto}"
        fi
    else
        printf "%s\n" "${colorRojo}El nombre del manager no puede estar vacío${colorDefecto}"
    fi
	pausar   
}
# Inicializar portainer en Swarm
inicializar_portainer_swarm(){
    # Obtenemos el nombre de la máquina
    read -p "Nombre de la máquina manager: " nombreMaquina
    # Comprobamos que haya introducido el nombre de la máquina
    comprobar_vacio $nombreMaquina
    if [[ $? -eq 0 ]]; then
            # Comprobamos que exista esa máquina en Docker Machine
            ipMaquina=$(docker-machine ip $nombreMaquina 2> /dev/null)
            if [[ $? -eq 0 ]]; then
                printf "%s\n" "${colorAzul}Inicializando Portainer en $nombreMaquina...${colorDefecto}"
                # Creamos la carpeta de certificados
                mkdir -p ./portainer/certificados
                # Creamos el certificado SSL de Portainer
                openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -subj "/C=ES/ST=Barcelona/L=Barcelona/O=Pedralbes/CN=$dominioPortainerPedralbes" -keyout portainer/certificados/portainer.key  -out portainer/certificados/portainer.crt
                # Nos conectamos al Docker Engine de la máquina
                eval $(docker-machine env $nombreMaquina)
                # Inicializamos Portainer
                docker stack deploy -c ./portainer/$portainerPedralbesYML pedralbes
                # Nos desconectamos del Docker Engine de la máquina
                eval $(docker-machine env -u)
                printf "%s\n" "${colorAzul}Portainer inicializado correctamente${colorDefecto}"
            else
                printf "%s\n" "${colorRojo}No existe $nombreMaquina en Docker Machine${colorDefecto}"    
            fi
    else
        printf "%s\n" "${colorRojo}El nombre de la máquina no puede estar vacío${colorDefecto}"
    fi
	pausar
}

# Mostrar el menú
mostrar_menu() {
	clear
    printf '%s\n%s\n%s\n' "${colorAzul}~~~~~~~~~~~~~~~~~~~~~" "MENÚ PRINCIPAL" "~~~~~~~~~~~~~~~~~~~~~${colorDefecto}"    
    printf "%s\n" "1. Mostrar máquinas en Docker Machine"
    printf "%s\n" "2. Generar clave ssh del master"
    printf "%s\n" "3. Copiar clave ssh a un nodo"
	printf "%s\n" "4. Añadir nodo a Docker Machine"
    printf "%s\n" "5. Corregir certificados de un Docker Machine"
    printf "%s\n" "6. Inicializar Swarm en un Docker Machine"
    printf "%s\n" "7. Añadir worker en Swarm"
    printf "%s\n" "8. Inicializar Portainer en Swarm"
    printf "%s\n" "9. Salir"
}
# Leemos la opción que escoge el usuario
leer_opcion(){
	local opcionEscogida
	read -p "Escoge una opción: " opcionEscogida
	case $opcionEscogida in
        1) mostrar_maquinas_dm ;;
        2) generar_claves_master ;;
        3) copiar_clave_master_nodo ;;
		4) anadir_nodo_a_dm ;;
		5) corregir_certificados_dm ;;
        6) inicializar_swarm ;;
        7) anadir_worker_swarm ;;
        8) inicializar_portainer_swarm ;;
		9) exit 0;;
		*) echo -e "${colorRojo}Escoga alguna opción${colorDefecto}" && sleep 1
	esac
}

# ----------------------------------------------
# Nos aseguramos que no acabe si pulsa CTRL+C, CTRL+Z, etc
# ----------------------------------------------
trap '' SIGINT SIGQUIT SIGTSTP

# -----------------------------------
# Mostramos el menú mientras no se escoja la opción de salir
# ------------------------------------
while true
do
	mostrar_menu
	leer_opcion
done
