**Nombre del proyecto:**

DockerPedralbes       

**Nombre de los integrantes del grupo**

~~~~
Andrés Jordán

Pedro Nieto

Tomás Silva
~~~~

**DockerPedralbes**

A través de nuestro proyecto, lo que queremos es una plataforma por ejemplo para el instituto que esté disponible, sea ágil y rápida. 
Un sistema de moodle que pueda ser bien gestionado y sin caídas. Y ya no solo la página web y el moodle del instituto sino el poder implementarlo en las materias como Implementación de Aplicaciones Web y “olvidar” el antiguo sistema de VirtualBox.
Por ejemplo en la materia anteriormente esmentada hace falta un servidor XAMPP para el funcionamiento de una página Web, en vez de montar una máquina virtual exclusivamente para esta función, podría haber un contenedor con este servidor ya instalado y que solo tenga que descargarlo y hacer el PHP del ejercicio que me piden, además este contenedor con mi PHP se podría subir y el profesor directamente podría corregirlo, aparte se podria subir igual un contenedor con la solución del ejercicio en un contenedor; y esto es solo un ejemplo específico de una materia concreta pero podemos ver que es un gran potencial a nivel de infraestructura.