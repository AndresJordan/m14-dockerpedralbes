#!/bin/bash
# ----------------------------------
# Variables
# ----------------------------------
nombreBox="mxlinux-docker"
ficheroBox="mxlinux-docker.box"
ficheroVagrant="Vagrantfile"
declare -A arrMasters=(["master"]="192.168.100.100")
# ----------------------------------
# Colores
# ----------------------------------
colorDefecto=$'\e[0m'
colorAzul=$'\e[1;34m'
colorRojo=$'\e[1;31m'
# ----------------------------------
# Funciones
# ----------------------------------
# Pausar el script hasta presionar [Enter]
pausar(){
	read -p "Presiona [Enter] para continuar..." fackEnterKey
}
# Comprobamos que el fichero exista y no sea un enlace simbolico
comprobar_fichero(){
    if [[ -e "$PWD/$1" && ! -L "$PWD/$1" ]]; then return 0; else return 1; fi
}
# Comprobamos que el box exista
comprobar_box(){
    printf "%s\n" "${colorAzul}Comprobando si existe el box $nombreBox...${colorDefecto}"
    vagrant box list | grep ^$nombreBox &> /dev/null
}
# Añadir el box
anadir_box(){
    comprobar_fichero $ficheroBox
    if [[ $? -eq 0 ]]; then
        comprobar_box
        if [[ $? -eq 0 ]]; then    
            printf "%s\n" "${colorRojo}El box $nombreBox está añadido${colorDefecto}"
        else
            printf "%s\n" "${colorAzul}Añadiendo el box de $nombreBox...${colorDefecto}"
            vagrant box add $nombreBox file://$ficheroBox
        fi
    else
        printf "%s\n" "${colorRojo}No se encuentra el fichero $ficheroBox en $PWD${colorDefecto}"
    fi
	pausar
}
# Inicializar el master
inicializar_master(){
    comprobar_box
    if [[ $? -eq 0 ]]; then
        comprobar_fichero $ficheroVagrant
        if [[ $? -eq 0 ]]; then
            printf "%s\n" "${colorAzul}Iniciando el master...${colorDefecto}"
            vagrant up
            printf '%s\n%s\n%s\n' "${colorAzul}~~~~~~~~~~~~~~~~~~~~~" "INFORMACIÓN MASTER" "~~~~~~~~~~~~~~~~~~~~~${colorDefecto}"
            (
            printf '%s\t\t%s\n' "Nombre master" "IP"
            for keyNombreMaster in "${!arrMasters[@]}"; do
                printf '%s\t\t%s\n' "$keyNombreMaster" "${arrMasters[$keyNombreMaster]}"
            done
            ) | column -t -s $'\t'    
        else
            printf "%s\n" "${colorRojo}No se encuentra el fichero $ficheroVagrant en $PWD${colorDefecto}"
        fi
    else
        printf "%s\n" "${colorRojo}No está el box $nombreBox añadido${colorDefecto}"
    fi
	pausar
}

# Destruir el master
destruir_master(){
    comprobar_fichero $ficheroVagrant
    if [[ $? -eq 0 ]]; then
        printf "%s\n" "${colorAzul}Destruyendo los boxes de $nombreBox...${colorDefecto}"
        vagrant destroy -f
    else
        printf "%s\n" "${colorRojo}No se encuentra el fichero $ficheroVagrant en $PWD${colorDefecto}"
    fi
	pausar
}

# Eliminar el box
eliminar_box(){
    comprobar_box
    if [[ $? -eq 0 ]]; then
        printf "%s\n" "${colorAzul}Removiendo el box $nombreBox...${colorDefecto}"
        vagrant box remove $nombreBox --force
    else
        printf "%s\n" "${colorRojo}No está el box $nombreBox añadido${colorDefecto}"
    fi
	pausar
}

# Mostrar el menú
mostrar_menu() {
	clear
    printf '%s\n%s\n%s\n' "${colorAzul}~~~~~~~~~~~~~~~~~~~~~" "MENÚ PRINCIPAL" "~~~~~~~~~~~~~~~~~~~~~${colorDefecto}"    
	printf "%s\n" "1. Añadir box"
    printf "%s\n" "2. Inicializar el master"
    printf "%s\n" "3. Destruir el master"
    printf "%s\n" "4. Eliminar box"
    printf "%s\n" "5. Salir"
}
# Leemos la opción que escoge el usuario
leer_opcion(){
	local opcionEscogida
	read -p "Escoge una opción: " opcionEscogida
	case $opcionEscogida in
		1) anadir_box ;;
		2) inicializar_master ;;
        3) destruir_master ;;
        4) eliminar_box ;;
		5) exit 0;;
		*) echo -e "${colorRojo}Escoga alguna opción${colorDefecto}" && sleep 1
	esac
}

# ----------------------------------------------
# Nos aseguramos que no acabe si pulsa CTRL+C, CTRL+Z, etc
# ----------------------------------------------
trap '' SIGINT SIGQUIT SIGTSTP

# -----------------------------------
# Mostramos el menú mientras no se escoja la opción de salir
# ------------------------------------
while true
do
	mostrar_menu
	leer_opcion
done
